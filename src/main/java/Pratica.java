
import java.lang.management.ManagementFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas
 */
public class Pratica {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        
        System.out.println("Sistema Operacional: " + System.getProperty("os.name"));
        System.out.println("Processadores disponíveis: " + rt.availableProcessors());
        System.out.println("Memória Total :" + rt.maxMemory()/1024);
        System.out.println("Memória Disponível :" + rt.freeMemory()/1024);
        System.out.println("Memória Utilizada: " + rt.totalMemory()/1024);
        
    }
}
